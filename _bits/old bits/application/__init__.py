from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_debugtoolbar import DebugToolbarExtension

# from flask_session import Session

# Globally accessible libraries
db = SQLAlchemy()
toolbar = DebugToolbarExtension()

def create_app():

    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    
    app.config.from_object('config.Config')
    db.init_app(app)
    toolbar.init_app(app)

    with app.app_context():

        # Include our Routes and import anything else
        from . import routes

        # Register Blueprints
        # app.register_blueprint(auth.auth_bp)

        # Create tables for our models
        db.create_all()

        return app