astroid==2.3.3
blinker==1.4
Click==7.0
colorama==0.4.1
Flask==1.1.1
Flask-DebugToolbar==0.10.1
Flask-DotEnv==0.1.2
Flask-Login==0.4.1
Flask-SQLAlchemy==2.4.1
Flask-WTF==0.14.2
gunicorn==20.0.4
isort==4.3.21
itsdangerous==1.1.0
Jinja2==2.10.3
lazy-object-proxy==1.4.3
MarkupSafe==1.1.1
mccabe==0.6.1
passlib==1.7.2
pylint==2.4.4
python-dotenv==0.10.3
six==1.13.0
SQLAlchemy==1.3.11
typed-ast==1.4.0
waitress==1.3.1
Werkzeug==0.16.0
wrapt==1.11.2
WTForms==2.2.1
