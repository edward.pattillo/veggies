# you need to make a copy of this file named "config.py" in the root folder of the app. Change the secret key. It's already been added to .gitignore

class Config:

    def __init__(self):
        self.dbName = "database/database.db"
        self.secret_key = "newSecret"