"""App configuration."""
from os import environ, path
# import shutil
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
    """Set Flask configuration vars from .env file."""

    # General Config
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_ENV = environ.get('FLASK_ENV')

    # if FLASK_ENV == "development":
    #     shutil.rmtree("application/static/.webassets-cache") 

    # Flask-Assets
    FLASK_ASSETS_USE_CDN = environ.get('FLASK_ASSETS_USE_CDN')
    # LESS_BIN = environ.get('LESS_BIN')
    # ASSETS_DEBUG = environ.get('ASSETS_DEBUG')
    # LESS_RUN_IN_DEBUG = environ.get('LESS_RUN_IN_DEBUG')

    # Static Assets
    STATIC_FOLDER = environ.get('STATIC_FOLDER')
    TEMPLATES_FOLDER = environ.get('TEMPLATES_FOLDER')
    COMPRESSOR_DEBUG = environ.get('COMPRESSOR_DEBUG')

    SQLALCHEMY_DATABASE_URI=environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS=environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')

    MAIL_SERVER=environ.get('MAIL_SERVER')
    MAIL_PORT=environ.get('MAIL_PORT')
    MAIL_USE_SSL=environ.get('MAIL_USE_SSL')
    MAIL_USERNAME=environ.get('MAIL_USERNAME')
    MAIL_PASSWORD=environ.get('MAIL_PASSWORD')

