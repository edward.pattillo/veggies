$(document).ready(function () {


    var path = window.location.pathname,
        link = window.location.href;

    // change active link colour
    // console.log(path,link)
    $('a[href="' + path + '"], a[href="' + link + '"]').parent('li').addClass('active');


    $('.slick-slides-home').slick({

        dots: false,
        // appendArrows: ,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3500,
        // fade: true,
        easing: 'ease',
        // mobileFirst: true,


        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
      });
});