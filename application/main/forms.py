from wtforms import Form, validators, TextField,TextAreaField, SubmitField
 
class ContactForm(Form):
  name = TextField("Name",  [validators.Required("Name required.")])
  email = TextField("Email",[validators.Required("Email required."), validators.Email("Email required.")])
  subject = TextField("Subject",  [validators.Required("Subject required.")])
  message = TextAreaField("Message", [validators.DataRequired("Message required.")])
  submit = SubmitField("Send")
