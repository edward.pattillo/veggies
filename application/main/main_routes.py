from flask import Blueprint, render_template, request, url_for, redirect, flash
from flask import current_app as app
from .forms import ContactForm
from flask_mail import Message, Mail
# from . import mailer



main_bp = Blueprint('main_bp', __name__,
                    template_folder='templates',
                    static_folder='static',
                    static_url_path='/main')



@main_bp.route('/')
def home():
    """Home page route."""
    return render_template('home.html')



@main_bp.route('/about')
def about():
    """About page route."""
    return render_template('about.html')



@main_bp.route('/organic')
def organic():
    """Organic page route."""
    return render_template('organic.html')



# this route handles the contact endpoint
@main_bp.route("/contact", methods=["GET", "POST"])
def contact():

    # this creates an object that contains the 
    # ContactForm fields and validation rules
    form = ContactForm(request.form)

    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        name = form.name.data
        email = form.email.data
        subject = form.subject.data
        message = form.message.data
        # submit = form.submit.data 


        msg = Message(subject, sender=email, recipients=['le.pattillo.nz@gmail.com'])
        msg.body = """
        From: %s <%s>
        %s
        """ % (name, email, message)

        with app.app_context():
            app.mailer.send(msg)
 

        # notify user and send them home
        flash("Message sent.", category="success")
        return redirect(url_for("main_bp.home"))
         
        
    elif request.method == "POST" and form.validate() == False:
        # if authentication failed 
        error = "Fields need filled in."
        # flash(error)
        return render_template("contact.html",form=form,error=error)

    # this is what happens when somone just visits the login
    # enpoint for the first time (before they submit something)
    return render_template("contact.html",form=form)

