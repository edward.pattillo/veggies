from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
from flask_debugtoolbar import DebugToolbarExtension
from flask_mail import Mail

from .admin import admin_routes
from .main import main_routes

# from flask_session import Session

# Globally accessible libraries
# db = SQLAlchemy()
toolbar = DebugToolbarExtension()
# mail = Mail()


def create_app():

    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    
    app.config.from_object('config.Config')
    # db.init_app(app)
    # toolbar.init_app(app)
    mail = Mail()
    mail.init_app(app)

    with app.app_context():
        # Include our Routes and import anything else
        # from . import routes
        app.mailer = mail

        # Register Blueprints

        """ The next argument we pass is a keyword argument called template_folder, which tells our Flask Blueprint which folder to render templates from in relation to the current file. Setting template_folder to templates means that our app will first look for a folder which matches /myapp/my-blueprint-folder/templates. If that folder doesn't exist, it will then try finding templates in /myapp/templates. This is important to remember- this behavior can get tricky very quickly.

        Setting static_folder mimics the behavior of setting template_folder, except for serving static assets.

        """
        # app.register_blueprint(auth.admin_bp)
        app.register_blueprint(admin_routes.admin_bp)
        app.register_blueprint(main_routes.main_bp)
 


        # Create tables for our models
        # db.create_all()

        return app